<?php
/**
 * Copyright (c) 2017 Cooperativa EITA (eita.org.br)
 * @author Vinicius Cubas Brand <vinicius@eita.org.br>
 * @author Daniel Tygel <dtygel@eita.org.br>
 *
 * This file is licensed under the Licensed under the MIT license:
 * http://opensource.org/licenses/MIT
 */

class MatrixOrg_RoomState {
	public $name;
	public $creatorUid;
	public $members = array();
	public $joinRule;
	public $aliases;
	public $powerLevels;
	public $historyVisibility;
	public $guestAccess;
	public $topic;
	public $avatarUrl;
	public $roomId;

	public $state_events;
	public $timeline_events;

	public function populate($result, $event_type='', $user_id='') {
		if ($result['status'] == 200) {
			if ($event_type) {
				$this->populateEvent($result['data'], $event_type, $user_id);
			} else {
				$this->populateEvents($result['data'], array());
			}
		}
	}

	public function populateEvents($state_events, $timeline_events, $room_id='') {
		if ($room_id) {
			$this->roomId = $room_id;
		}

		$this->state_events = json_decode(json_encode($state_events));
		foreach ($state_events as $event) {
			$this->populateEvent($event['content'],$event['type'], $event['state_key']);
		}

		$this->timeline_events = json_decode(json_encode($timeline_events));
		foreach ($timeline_events as $event) {
			$this->populateEvent($event['content'],$event['type'], $event['state_key']);
		}
	}

	public function populateEvent($content, $event_type, $user_id='') {
		switch ($event_type) {
			case "m.room.join_rules":
				$this->joinRule = $content['join_rule'];
				break;
			case "m.room.avatar":
				$this->avatar = $content['url'];
				break;
			case "m.room.name":
				$this->name = substr($content['name'],0,64);
				break;
			case "m.room.member":
				$this->members[$user_id]['membership']=$content['membership'];
				switch($content['membership']) {
					case 'join':
					case 'invite':
						$this->members[$user_id]['user_id']=$user_id;
						$this->members[$user_id]['membership']=$content['membership'];
						$this->members[$user_id]['displayname']=$content['displayname'];
						break;
					case 'leave':
						unset($this->members[$user_id]);
						break;
				}
				break;
			case "m.room.history_visibility":
				$this->historyVisibility = $content['history_visibility'];
				break;
			case "m.room.create":
				$this->creatorUid = $content['creator'];
				break;
			case "m.room.power_levels":
				$this->powerLevels = $content;
				foreach ($content['users'] as $uid=>$powerLevel) {
					$this->members[$uid]['power_level'] = $powerLevel;
				}
				break;
			case "m.room.guest_access":
				$this->guestAccess = $content['guest_access'];
				break;
			case "m.room.topic":
				$this->topic = $content['topic'];
				break;
			case 'm.room.aliases':
			case 'm.room.canonical_alias':
				break;
			default:
				#throw new MatrixOrg_Exception("Populating MatrixOrg RoomState object: error: unknown event type. event_type=$event_type content=".json_encode($content));
		}
	}

	public function isMember($user_id) {
		return array_key_exists($user_id,$this->members);
	}
}